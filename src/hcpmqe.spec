# -*- mode: python -*-

import sys

if sys.platform == 'darwin':
    _pathext = ['.']
    _name = 'hcpmqe.macos'
elif sys.platform == 'win32':
    _pathext = ['.']
    _name = 'hcpmqe.exe'
elif sys.platform == 'linux':
    _pathext = ['.']
    _name = 'hcpmqe.linux'
else:
    sys.exit('{} is not supported, yet'.format(sys.platform))

block_cipher = None


a = Analysis(['hcpmqe.py'],
             pathex=_pathext,
             binaries=[],
             datas=[],
             hiddenimports=['hcpmqelib.__init__', 'hcpmqelib.__main__', 'hcpmqelib', 'hcpmqelib.conf', 'hcpmqelib.db',
                            'hcpmqelib.log', 'hcpmqelib.mqe', 'hcpmqelib.version'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name=_name,
          debug=False,
          strip=False,
          upx=False,
          console=True if sys.platform == 'win32' else False,
          )
