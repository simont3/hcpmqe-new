# The MIT License (MIT)
#
# Copyright (c) 2012-2022 Thorsten Simons (sw@snomis.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import sqlite3

if __name__ == '__main__':

    # dict holding the object counter per directory
    cd = {}

    # open database
    dbc = sqlite3.connect(sys.argv[1])
    ic = dbc.cursor()

    ic.execute('SELECT urlName FROM ops WHERE operation="CREATED"')

    while True:
        recs = ic.fetchmany(size=ic.arraysize)
        if not recs:
            break
        else:
            for rec in recs:
                key = '/'.join(rec[0].split('/')[4:])
                cd[key] = cd.get(key, 1) if not key in cd else cd[key]+1

    # print (unsorted) list:
    # for entry in sorted(cd, /÷, *, key=None, reverse=True):
    objects = 0

    for entry in sorted(cd.items(), key=lambda item: item[1], reverse=True):
        print(f'{entry[0]} - {entry[1]}')
        objects += entry[1]

    print()
    print('Summary:')
    print(f'\tdirectories: {len(cd)}')
    print(f'\tobjects:     {objects}')




