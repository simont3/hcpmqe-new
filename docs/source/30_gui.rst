User Interface
==============

Main panel
----------

    ..  image:: _static/gui.png
        :scale: 50%

HCP access parameters
+++++++++++++++++++++

    ..  image:: _static/access_params.png
        :scale: 75%

Here, the HCP system to query is addressed. Either the system can be addressed entirely (FQDN starting
with *admin.*) or a specific Tenant (FQDN starting with the Tenants name) can be addressed.

The query can optionally be re-fined by specifying one (or more) Namespaces, separated by comma.

..  Note::

    Please note that Namespaces **must always** be specified as *Namespace.Tenant*, even in case a specific
    Tenant is queried!

Further refining is available by specifying one (or more) directories (starting with */*, and separated
by comma). Please note that directories specified will be used for each and every Namespace addressed by
the query.

The user specified must be a local HCP user (no AD account) with the proper permissions granted, as
described in the prior chapter.

HCP load parameters
+++++++++++++++++++

    ..  image:: _static/load_params.png
        :scale: 75%

MQE queries can produce a huge amount of records to be fetched from HCP, depending on the number of objects
addressed by the query. Therefore, paged queries of up to 10,000 records are used to keep the peak load in
an acceptable range.

A throttle of up to 60 seconds can be tuned in to relax the load on HCP even more, at the cost of a longer
query run time.

In case timeout errors are reported, try a longer request timeout than the default 60 seconds.

..  Tip::

        The values (except for timeout) can be changed while a query is running. Use the slider to change the
        value, then click the **[Set]** button. The new value will be picked up with the next page request.

Output parameters
+++++++++++++++++

    ..  image:: _static/output_params.png
        :scale: 75%

Supported output types are comma-separated-value (csv-) files, plain as well as compressed (bz2, gzip, lzma),
and Sqlite3 database files.

Selecting *verbose* will request *all* system metadata values per object from HCP, while not selecting it will
request just the bare minimum (4 fields) that clearly identifies each object and the operation that
triggered the record)

Query parameters
++++++++++++++++

    ..  image:: _static/query_params.png
        :scale: 75%

The operations (transactions) to be queried for:

    *   *create* - list all actually existing objects and their versions ingested
    *   *delete* - list all objects and object versions deleted
    *   *dispose* - list all objects deleted by disposition (automatic delete when retention period
        ends)
    *   *prune* - list all object versions automatic deleted when the configured version life span
        ended
    *   *purge* - list object versions deleted along with the objects actual version

Note that only objects / versions are returned where the respective operation happened during the selected
time frame. Also, note that -depending on HCP configuration- records of deleted objects / versions are held
for a limited number of days, only.


Time range
++++++++++

    ..  image:: _static/time_range.png
        :scale: 75%

Defines the time range for which operations are requested.

Status
++++++

    ..  image:: _static/status.png
        :scale: 75%

The *Status* line tells what's going on, *Records found* informs about how many records (object operations)
have been returned so far.
The *Last record* block tells about the identity of the last found record. These values can be used to restart
an interrupted quuery, for example. See the following recipies chapter.

Last Record
+++++++++++

    ..  image:: _static/last_record.png
        :scale: 75%

This area displays the last record received. It is either the last record within a received page (as long
as a query is running), or the final record received during the query.

..  Note::

    The configuration file is auto-updated with these values after every page received successfully, to allow to
    continue with a query later on from exactly that position.

    That means that a query can always be repeated or extended from that position - if a query finished
    successfully, if a query was canceled before finished, or if even the tool crashed.

Time bar
++++++++

    ..  image:: _static/time_bar.png
        :scale: 75%

During and after a query, the time bar shows the overall run time, the time spent on page queries as
well as the time spent on writing the database (or csv file).
