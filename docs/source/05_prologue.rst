Prologue / Intro
================

There are situations where one needs to have information about all the objects stored in an
Object Storage system, or even what has happened to an object during its lifetime. As well,
sometimes one needs to find out if an object has been stored and later on deleted.

In general there is few abilities, beside of 'walking the tree', to get this kind of information
from most of the Object Storage systems on the market.

For Hitachi Content Platform (HCP), things are different. HCP offers a built-in *metadata query engine*
(MQE), which is able to provide the mentioned details.

The tool described in this document is using the *MQE API* to request information about object-related
operations from HCP. Object-related operation means records describing what and when things happened
to an object: it's creation, metadata changes as well as deletion (including disposition, prune and
purge operations).

Is output is either a sqlite3 database file or comma-separated-value (csv) file, plain or compressed,
holding a list of the requested operations.

Using the query options, it can answer questions like:

    *   which objects are in the system?
    *   which objects were deleted during a given time period?
    *   etc.

Some recipes for using the acquired data can be found in the *recipies* chapter.

