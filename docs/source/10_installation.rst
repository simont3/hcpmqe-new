Installation
============

First of all, be aware that **hcpmqe** is a GUI-based tool. It will *not* run on a system without a
GUI (headless Linux, for example).

Second, no binary installers are provided, due to the labor required to make it happen reliably for
all platforms supported. A Python 3.7 (or newer) installation is required.


Installing the Python package
-----------------------------

Given that Python 3 is installed, the process of installing **hcpmqe** is pretty straight forward.
It's highly suggested to use a Python virtual environment, especially if the tools is used as a one-off.

..  Note::

    **Internet access is required** to be able to install the package, as it depend on other packages
    to be loaded from `PyPi (the Python package index) <https://pypi.org>`_\.

This is how to do it:

*   Check the Python version:

        ::

            $ python3 --version
            Python 3.7.4

    Note: Python >= 3.7 is required, any higher version should do as well.


*   Create a folder to work in:

        ::

            $ mkdir hcpmqe
            $ cd hcpmqe

*   Setup a Python virtual environment:

        ::

            $ python3 -m venv .venv

*   Activate the virtual environment:

        Linux, macOS::

            $ source .venv/bin/activate
            (.venv) $

        Windows::

            C:\Users\sm\hcpmqe> .venv\Scripts\activate
            (.venv) C:\Users\sm\hcpmqe>

    Noticed the changed prompt? This shows that you have activated the virtual environment.

*   Update the Python setup tools:

        ::

            (.venv) $ pip install -U pip setuptools
            [.. a lot of messages shown here ..]
            Successfully installed pip-19.2.2 setuptools-41.2.0

*   Install the tools python package:

        ::

            (.venv) $ pip install hcpmqe
            Collecting hcpmqe
              Downloading hcpmqe-2.0.2.tar.gz (17 kB)
            Collecting PySimpleGUI==4.30.0
              Using cached PySimpleGUI-4.30.0-py3-none-any.whl (233 kB)
            Collecting httpx==0.16.1
              Using cached httpx-0.16.1-py3-none-any.whl (65 kB)
            Collecting certifi
              Using cached certifi-2020.11.8-py2.py3-none-any.whl (155 kB)
            Collecting httpcore==0.12.*
              Downloading httpcore-0.12.1-py3-none-any.whl (54 kB)
                 |--------------------------------| 54 kB 968 kB/s
            Collecting rfc3986[idna2008]<2,>=1.3
              Using cached rfc3986-1.4.0-py2.py3-none-any.whl (31 kB)
            Collecting sniffio
              Using cached sniffio-1.2.0-py3-none-any.whl (10 kB)
            Collecting h11==0.*
              Using cached h11-0.11.0-py2.py3-none-any.whl (54 kB)
            Collecting idna; extra == "idna2008"
              Using cached idna-2.10-py2.py3-none-any.whl (58 kB)
            Using legacy 'setup.py install' for hcpmqe, since package 'wheel' is not installed.
            Installing collected packages: PySimpleGUI, certifi, sniffio, h11, httpcore, idna, rfc3986, httpx, hcpmqe
                Running setup.py install for hcpmqe ... done
            Successfully installed PySimpleGUI-4.30.0 certifi-2020.11.8 h11-0.11.0 hcpmqe-2.0.2 httpcore-0.12.1 httpx-0.16.1 idna-2.10 rfc3986-1.4.0 sniffio-1.2.0

Now you can run the tool as described in the following chapters, by just calling ``hcpmqe``.

..  Note::

    Please keep in mind that you **need to have the Python virtual environment activated** to be able
    to run the tool. If in need, simply activate it by running:

        ::

            $ cd hcpmqe
            $ source .venv/bin/activate

    or

        ::

            C:\Users\sm> cd hcpmqe
            C:\Users\sm\hcpmqe> .venv\Scripts\activate
