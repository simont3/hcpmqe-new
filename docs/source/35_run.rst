Run queries
===========

Preparation
-----------

You **need to have the Python virtual environment (created during install) activated** to be able
to run the tool. If in need, simply activate it by running:

    Linux, macOS::

        $ cd hcpmqe
        $ source .venv/bin/activate

or

    Windows::

        C:\Users\sm> cd hcpmqe
        C:\Users\sm\hcpmqe> .venv\Scripts\activate


Start the tool
--------------

    ::

        $ hcpmqe --help
        usage: hcpmqe [-h] [--version] [-C]

        optional arguments:
          -h, --help         show this help message and exit
          --version          show program's version number and exit
          -C, --log2console  instead of logging to hcpmqe.log, log to console

The tool always logs its doings - either into a file in the current directory (*hcpmqe.<pid>.log*),
or, if the ``-C`` argument is used, to the console.

Running the command will open the GUI:

    ..  image:: _static/run_start_main.png
        :scale: 50%


Run a query from scratch
------------------------

Once the form is filled with parameters matching the wanted query, save the configuration, then click the
[**Run query**] button to start the process.

    ..  image:: _static/run_start_query.png
        :scale: 50%

All the entry fields will be disabled, except the ones that allows to change page size and
throttle. The *Status* line will show progress information, *Records found* reports the no. of
records received so far, the *Last record* section shows the identity of the last pages final
record, and in the very bottom, some timing information is displayed.


Re-start a query
----------------

If a query was canceled or interrupted for whatever reason, it can be restartet.
If the tool crashed or was killed somehow, just start it again and load the configuration file.
It will show information about the last record that was written to the output file.

Do **not change any (!!!) parameter** and press [**Run query**] again (changing values will likely cause the
query to end up incomplete). You'll be asked if you want to continue or start from scratch.

    ..  image:: _static/run_start_restart_query.png
        :scale: 50%


