HCP prerequisites
=================

..  Warning::

        Not having the proper permissions and/or the MQE API being disabled will always lead to
        *error 403* when running a query:

            ..  image:: _static/HCP_status403.png
                :scale: 50%

MQE API
-------

HCP needs to have the metadata query API enabled to allow the tool to function. The minimal
setting needed can be set using the *HCP System Console > Services > Search* panel:

    ..  image:: _static/HCP_search.png
        :scale: 50%

**Enable metadata query API** is the **only** setting required in **this** panel.

System administrator
--------------------

A system-level administrator **must** at least have the **Search** role to access the MQE API:

    ..  image:: _static/HCP_mqeuser.png
        :scale: 50%


Such an administrator is able to query Tenants that have granted system-level users to manage the tenant and
search its namespaces in the respective *Tenant Console > Overview* panel:

    ..  image:: _static/HCP_allowadmin.png
        :scale: 50%

As a result of this, a full system-wide list of all operations can only be acquired if **all** Tenants have
granted this privilege.

Using an HCP FQDN starting with *"tenantname."* will query just that Tenant. In this case, the *data network*
configured for the Tenant must be reachable by the tool, and its FQDN must be resolvable via DNS.

Using an HCP FQDN starting with *"admin."* will query all Tenants that have granted the permission, even if
the configured data network for some of the Tenants are not reachable by the tool.



Tenant user
-----------

A Tenant user **must** have at least the **Search** permission for the Namespace(s) he shall query:

    ..  image:: _static/HCP_tenantuser.png
        :scale: 50%

Of course, the tool must be able to reach the configured *data network* of the Tenant, and its FQDN must
be resolvable via DNS.

In addition to that, **Search** needs to be enabled for any Namespaces that shall be queried:

    ..  image:: _static/HCP_Namespace_Search.png