Database Schema
===============

The schema of the ``ops`` database table, containing the collected operation records, differs between
verbose or non-verbose query mode.

In addition, the database schema is build dynamically from the metadata keys HCP returns; that said,
there might be slight differences between HCP versions. As of now (April 2022), this has been just added
keys. Nevertheless, if such a change happens during an HCP version upgrade, the database in use might not
be usable with the newer version of HCP, and thus needs to be created from scratch (*just delete the
existing database and run a new query*).

Here are samples of the ``ops`` tables schema:

Non-verbose mode
----------------

..  code-block:: none

                      Column | example value
    -------------------------+-------------------------------------------------
      changeTimeMilliseconds | 1648129832613.00
                   operation | CREATED
                     urlName | https://one.s3.hcp80.archivas.com/rest/hallo.txt
                     version | 105480309271425


Verbose mode
------------

..  code-block:: none

                      Column | example value
    -------------------------+-------------------------------------------------
                  accessTime | 1648129832
            accessTimeString | 2022-03-24T14:50:32+0100
                         acl | 0
      changeTimeMilliseconds | 1648129832613.00
            changeTimeString | 2022-03-24T14:50:32+0100
              customMetadata | 1
    customMetadataAnnotation | .metapairs
                         dpl | 1
                         gid | 0
                        hash | SHA-256 78FC...<cut>...232F
                  hashScheme | SHA-256
                        hold | 0
                      _index | 1
                  ingestTime | 1648129832
            ingestTimeString | 2022-03-24T14:50:32+0100
                   namespace | one.s3
                  objectPath | /hallo.txt
                   operation | CREATED
                       owner | USER,s3,s3
                 permissions | 555
                  replicated | 0
        replicationCollision | 0
                   retention | 0
              retentionClass |
             retentionString | Deletion Allowed
                       shred | 0
                        size | 6
                        type | object
                         uid | 0
                  updateTime | 1648129832
            updateTimeString | 2022-03-24T14:50:32+0100
                     urlName | https://one.s3.hcp80.archivas.com/rest/hallo.txt
                    utf8Name | hallo.txt
                     version | 105480309271425

